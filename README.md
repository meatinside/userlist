# User List Angular Demo

## Prerequirements

* Start MongoDB server
* Run `npm i` to install dependencies

## Starting application

* Run `ng build && node server.js`
* Open `http://localhost:3000/` in browser

## Stack

* TypeScript
* Angular 5.2.0
* NodeJS 9.3.0
* Express
* MongoDB

## Features

* Application use a stub API for user service
* Stub API use database with 5000 random users generated via http://randomuser.me
* List uses infinite scroll, thats why only nextPageUrl is used at the moment
* List refreshes automatically by typing in search field with debouncing
* There is no tests at the moment
