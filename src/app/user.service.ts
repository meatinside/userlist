import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { UserSearchResult } from "./models/user";

const URL_PREFIX = "http://localhost:3000/api/users/?searchTerm="; // TODO: move it to config

@Injectable()
export class UserService {
   constructor(private http: Http) {
   }

   getUsers(searchTerm: string, url?: string): Observable<UserSearchResult> {
      if(!url) {
        url = URL_PREFIX+searchTerm;
      }
      return this.http.get(url)
         .map((res: Response) => res.json())
         .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
   }
}
