export class User {
   constructor(
      public id: string,
      public name: string,
      public avatarUrl: string
   ) {}
}

export class UserSearchResult {
  constructor(
    public result: User[],
    public nextPageUrl: string,
    public previousPageUrl: string
  ) {}
}
