import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  users = null
  nextPageUrl = null
  searchTermSubject = new Subject <string>();

  ngOnInit() {
    this.search('')
  }

  more() {
    this.search(null, this.nextPageUrl);
  }

  searchChanged(value) {
    this.searchTermSubject.next(value);
  }

  search(searchTerm: string, url?: string) {
    this.userService.getUsers(searchTerm, url).subscribe((data) => {
      this.nextPageUrl = data.nextPageUrl;
      if(url) {
        this.users.push(...data.result);
      } else {
        this.users = data.result.length ? data.result : null;
      }
    });
  }

  constructor(private userService: UserService) {
    this.searchTermSubject
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe((searchTerm)=>this.search(searchTerm))
  }
}
