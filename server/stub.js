const User = require('./models/user');
const users = require('./users.json');

const createUsers = () => {
  console.log("Adding stub users");
  return User.remove({}).then((result) => {
    return User.create(users);
  }).catch((err) => {
    console.log("Error: "+err);
  });
};

module.exports = createUsers;
