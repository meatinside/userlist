const express = require('express');
const router = express.Router();
const User = require('../models/user');

const USERS_PER_PAGE = 10;
const API_SERVER_URL = "http://localhost:3000/api"; // TODO: move it to config

function getUrl(searchTerm, page) {
  return API_SERVER_URL+`/users/?searchTerm=${searchTerm}&page=${page}`;
}

router.get('/users', (req, res) => {
  var searchTerm = req.query.searchTerm || '';
  var page = parseInt(req.query.page) || 0;
  var limit = USERS_PER_PAGE;
  var skip = USERS_PER_PAGE*page;
  var query = {name: new RegExp(searchTerm, 'i')};

  User.find(query, [], {sort: {'id':1}}, (err, result) => {
    User.count(query, (err, count) => {
      let previousPageUrl = page ? getUrl(searchTerm, page-1) : null;
      let nextPageUrl = (page < Math.ceil(count/USERS_PER_PAGE) - 1) ? getUrl(searchTerm, page+1) : null;
      res.send({
        result,
        nextPageUrl,
        previousPageUrl
      });
    })

  }).skip(skip).limit(limit).catch((err)=>{
    console.log(err);
  });

});

router.get('/user', (req, res) => {
  res.send('api works');
});

module.exports = router;
